import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import { ApiService } from 'src/app/services/api.service';
import ApiResponse from 'src/app/models/ApiResponse';


@Component({
  selector: 'app-catagory',
  templateUrl: './catagory.component.html',
  styleUrls: ['./catagory.component.css']
})
export class CatagoryComponent implements OnInit {
token:string;
file:File;
image : any;
img:string;
availableCategories: any[];

category={
  name:'',
  media:''
  
}
  constructor(private localStorage:LocalStorageService, private apiService:ApiService) { }

  ngOnInit() {
    this.token= this.localStorage.get('userToken');
    this.getCat();
  }
  getCat=()=>{
    this.apiService.getCategoryList(this.token).subscribe((response:ApiResponse) =>{
    this.availableCategories=response.data;
   
     } );
  }
  addCategory = () => {
    this.apiService.addCategory(this.category, this.token).subscribe((response: ApiResponse) => {
      this.availableCategories.push(response.data);
       
      // console.log(response);
    });
  };
  delete=(id)=>{
   
    this.apiService.deleteCategoryList({id:id},this.token).subscribe(()=>
    {
            this.getCat();
    });

  }
  status=false;
  fileSelected = (e) => {
    this.file = e.target.files[0];
    const reader = new FileReader();
    reader.onload = (e:any) => {
      this.image = e.target.result;
      this.status=true;
    };
    reader.readAsDataURL(this.file);
   // this.status =false;
    

  };

  upload = (e) => {
    console.log(e.target);
    let fd = new FormData();
    console.log(this.file);
    fd.append('file', this.file);
    fd.append('testData', 'something');
    this.apiService.uploadImage(this.token, fd).subscribe((response:ApiResponse) => {
      console.log(response);
      this.category.media = response.data.id;
      alert(response.data.id);
      
     this.status=false;
    });
   
    // alert('works');
  };

}
