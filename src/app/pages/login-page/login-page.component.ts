import {Component, OnInit, Injectable} from '@angular/core';
import ApiResponse from '../../models/ApiResponse';
import {Router} from '@angular/router';
import {ApiService} from '../../services/api.service';
import {LoginRequest} from '../../models/requestModel/login';
import {LocalStorageService} from 'angular-2-local-storage';
import {animate, state, style, transition, trigger, useAnimation} from '@angular/animations';
import {shake} from 'ng-animate';
import{AuthService} from '../../services/auth.service'
import { AngularFireAuth } from 'angularfire2/auth';
// import * as firebase from 'firebase/app';
// import { Observable } from 'rxjs';

@Injectable()


@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css'],
  animations: [
    trigger('credCheck', [
      transition('neutral => invalid', [useAnimation(shake, {params: {timing: .5}})])
    ])//animation done
  ]
})
export class LoginPageComponent implements OnInit {
  flag: any = {
    callingApi: false,
    invalidCred: false
  };
  user: LoginRequest = {
    username: '', //bhagirathi
    password: '' //papun1993
  };

  
  constructor(private router: Router, private apiService: ApiService, private localStorage: LocalStorageService,private authlogin:AuthService) {
    
  }

  ngOnInit() {
  }

  login = () => {
    if (this.flag.callingApi) {return; }
    this.flag.callingApi = true;
    this.apiService.login(this.user).subscribe((response: ApiResponse) => {
      this.flag.callingApi = false;
      if (response.status !== 'OK') {
        this.flag.invalidCred = true;
        setTimeout(() => {
          this.flag.invalidCred = false;
        },  600);
        return;
      }

       this.localStorage.set('userToken', response.data.token);
      this.localStorage.set('userdtl',response.data.name);
      this.localStorage.set('userLoggedIn', true);
      this.router.navigate(['home']);
    });
  }
  loginwithgoogle(){
    this.authlogin.signinwithgoogle()
    // return this._firebaseAuth.auth.signInWithPopup(
    //   new firebase.auth.GoogleAuthProvider()
    // )
    
  }
}
