import { Component, OnInit } from '@angular/core';
import{ApiService}  from'../../services/api.service';
import {LocalStorageService} from 'angular-2-local-storage';
import ApiResponse from 'src/app/models/ApiResponse';

  import { from } from 'rxjs';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {
token:string;
file:File;
image : any;

  constructor( private apiService:ApiService , private localStorage:LocalStorageService) { }

  ngOnInit() {
    this.token= this.localStorage.get('userToken');
  }
  status=false;
  fileSelected = (e) => {
    this.file = e.target.files[0];
    const reader = new FileReader();
    reader.onload = (e:any) => {
      this.image = e.target.result;
      this.status=true;
    };
    reader.readAsDataURL(this.file);
   // this.status =false;
    

  };

  upload = (e) => {
    console.log(e.target);
    let fd = new FormData();
    console.log(this.file);
    fd.append('file', this.file);
    fd.append('testData', 'something');
    this.apiService.uploadImage(this.token, fd).subscribe((response:ApiResponse) => {
      console.log(response);
      alert(response.data.id);
     this.status=false;
    });
   
    // alert('works');
  };


}
