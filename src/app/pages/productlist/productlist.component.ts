import { Component, OnInit } from '@angular/core';
import {LocalStorageService} from 'angular-2-local-storage';
import{ApiService} from '../../services/api.service';
import ApiResponse from 'src/app/models/ApiResponse';
import { Response } from 'selenium-webdriver/http';


@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.css']
})
export class ProductlistComponent implements OnInit {
  token:string;
  product:any[];
  productData:any={
    limit:'',
    page:'',
    category:''
  }
  constructor(private localStorage:LocalStorageService ,private apiService:ApiService) { }

  ngOnInit() {
    this.token = this.localStorage.get('userToken');
   this.getproduct();
   console.log(this.token);  
  }
   getproduct=()=>{
     this.apiService.getProduct(this.token).subscribe((response:ApiResponse)=>
     {
      
       this.product = response.data;
       
     });
    
   };
   deleteproduct=(id)=>{
     this.apiService.deleteProduct({id:id}, this.token).subscribe((Response:ApiResponse)=>{
      
      this.getproduct();
      alert("Product remove sucessfully");
       });
       
   };

}
