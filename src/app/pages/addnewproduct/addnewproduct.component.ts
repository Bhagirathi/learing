import { Component, OnInit } from '@angular/core';
import { productRequest } from 'src/app/models/requestModel/productRequest';
import {ApiService} from '../../services/api.service';
import ApiResponse from '../../models/ApiResponse';
import { LocalStorageService } from 'angular-2-local-storage';
import { Response } from 'selenium-webdriver/http';

@Component({
  selector: 'app-addnewproduct',
  templateUrl: './addnewproduct.component.html',
  styleUrls: ['./addnewproduct.component.css']
})
export class AddnewproductComponent implements OnInit {

  // flags: any = {
  //   fetchingCategory: false
  // };
  product:productRequest={
    name:'',
    price:'',
    description:'',
    category:'0'
    
  }
    
  availableCategories: any[];
 availableProduct:any[];
token:string;
  
  constructor(private apiService: ApiService,private localStorage:LocalStorageService) { }

  ngOnInit() {
    this.token= this.localStorage.get('userToken');
    this.getCat();
  }
  getCat=()=>{
    this.apiService.getCategoryList(this.token).subscribe((response:ApiResponse) =>{
    this.availableCategories=response.data;
   
     } );
    }
    addProduct=()=>{
      this.apiService.addProduct(this.product,this.token).subscribe((response:ApiResponse)=>
      {
        this.product={
          name:'',
          price:'',
          description:'',
          category:'0'
        }
        this.availableProduct=response.data;
        alert("product insert sucessfully");
      });
    }
}
