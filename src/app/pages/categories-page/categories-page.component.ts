import {Component, OnInit} from '@angular/core';
import {LocalStorageService} from 'angular-2-local-storage';
import {ApiService} from '../../services/api.service';
import ApiResponse from '../../models/ApiResponse';

@Component({
  selector: 'app-categories-page',
  templateUrl: './categories-page.component.html',
  styleUrls: ['./categories-page.component.css']
})
export class CategoriesPageComponent implements OnInit {
  token: string;
  availableCategories: any = [];
  category: any = {
    name: '',
    media: '5c6e721e1e74ee184c279012'
  };

  constructor(private localStorage: LocalStorageService, private apiService: ApiService) {
  }

  ngOnInit() {
    this.token = this.localStorage.get('userToken');
    this.getCat();
  }
  getCat = ()=>{
    this.apiService.getCategoryList(this.token).subscribe((response: ApiResponse) => {
      this.availableCategories = response.data;
    });
  }
  addCategory = () => {
    this.apiService.addCategory(this.category, this.token).subscribe((response: ApiResponse) => {
      this.availableCategories.push(response.data);
      // console.log(response);
    });
  };
}
