import {Component, OnInit} from '@angular/core';
import * as $ from 'jquery';
import {LocalStorageService} from 'angular-2-local-storage';
import {Router} from '@angular/router';
import {ApiService} from '../../services/api.service';
import ApiResponse from '../../models/ApiResponse';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  user_dtl:any=[];
  dept_dtl:any[];
  token:string;
  uname:string;
  contact:string;
  dept:string;
  constructor(private localStorage: LocalStorageService, private router: Router,private apiService:ApiService ) {
  }

  ngOnInit() {
    this.token=this.localStorage.get('userToken');
    this.userdtl();
  }
userdtl=()=>{
  this.apiService.userdtl(this.token).subscribe((response:ApiResponse)=>{
    this.user_dtl = response.data;
    
    this.uname=this.user_dtl.name;
    this.contact=this.user_dtl.contact;
    this.dept=this.user_dtl.department.name;
    
    return;

    
  });
}
  logout = () => {
    this.localStorage.clearAll();
    $('.modal-backdrop').hide();
  }

}
