import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {RegisterRequest} from '../../models/requestModel/register';
import {ApiService} from '../../services/api.service';
import ApiResponse from '../../models/ApiResponse';

@Component({
  selector: 'app-registration-page',
  templateUrl: './registration-page.component.html',
  styleUrls: ['./registration-page.component.css']
})
export class RegistrationPageComponent implements OnInit {
  flags: any = {
    fetchingDepartments: false
  };
  user: RegisterRequest = {
    department: '0',
    contact: '',
    name: '',
    password: '',
    username: ''
  };
  confPass: string;
  departments: Department[];

  constructor(private apiService: ApiService, private router: Router) {
  }

  ngOnInit() {
    console.log('calling api');
    this.flags.fetchingDepartments = true;
    this.apiService.getDepartments().subscribe((response: any) => {
      if (response.status === 'OK') {
        this.flags.fetchingDepartments = false;
        this.departments = response.data;
      } else {
        alert('Unable to get departments');
      }
    });
  }

  signUp = () => {
    if (this.user.password !== this.confPass) {
      return alert('Password Not Match');
    }
    this.apiService.register(this.user).subscribe((response: ApiResponse) => {
      if (response.status === 'OK') {
        this.router.navigate(['home']);
      } else {
        alert(response.message);
      }
    });
  };

}
