import {Component, OnInit} from '@angular/core';
import {LocalStorageService} from 'angular-2-local-storage';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.css']
})
export class DashboardPageComponent implements OnInit {
  token: string;
  username:string;

  constructor(private localStorage: LocalStorageService) {
  }

  ngOnInit() {
    this.token = this.localStorage.get('userToken');
    this.username= this.localStorage.get('userdtl');
    console.log(this.localStorage.get("userdtl"))
  }

}
