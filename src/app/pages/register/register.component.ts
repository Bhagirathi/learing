import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../services/api.service';
import {LocalStorageService} from 'angular-2-local-storage';
import ApiResponse from 'src/app/models/ApiResponse';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
token:string;
  constructor(private apiService:ApiService,private localStorage:LocalStorageService) { }

  ngOnInit() {
    this.token = this.localStorage.get('userToken');
  }

}
