import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { RegistrationPageComponent } from './pages/registration-page/registration-page.component';
import { ForgetPasswordPageComponent } from './pages/forget-password-page/forget-password-page.component';
import {RouterModule} from '@angular/router';
import {MainRoutingModule} from './routes/mainRouter';
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { ButtonPageComponent } from './pages/button-page/button-page.component';
import {TooltipModule} from 'ng2-tooltip-directive';
import {HttpClientModule} from '@angular/common/http';
import {LocalStorageModule} from 'angular-2-local-storage';
import { CategoriesPageComponent } from './pages/categories-page/categories-page.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { Component1Component } from './component/component1/component1.component';
import { Component2Component } from './component/component2/component2.component';
import { ChatPageComponent } from './pages/chat-page/chat-page.component';
import { CatagoryComponent } from './pages/catagory/catagory.component';
import { AddnewproductComponent } from './pages/addnewproduct/addnewproduct.component';
import { ProductlistComponent } from './pages/productlist/productlist.component';
import { RegisterComponent } from './pages/register/register.component';
import { FileUploadComponent } from './pages/file-upload/file-upload.component';
import { DemoComponent } from './demo/demo.component'; //import for animation
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import{AuthService} from './services/auth.service';
import { environment } from '../environments/environment';
import { GmapComponent } from './pages/gmap/gmap.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    RegistrationPageComponent,
    ForgetPasswordPageComponent,
    DashboardPageComponent,
    HomePageComponent,
    ButtonPageComponent,
    CategoriesPageComponent,
    Component1Component,
    Component2Component,
    ChatPageComponent,
    CatagoryComponent,
    AddnewproductComponent,
    ProductlistComponent,
    RegisterComponent,
    FileUploadComponent,
    DemoComponent,
    GmapComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    FormsModule,
    TooltipModule,
    HttpClientModule,
    MainRoutingModule,
    LocalStorageModule.forRoot({
      storageType: 'localStorage',
      prefix: ''
    }),
    AngularFireModule.initializeApp(environment.firebase, 'angular-auth-firebase'),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
   
    BrowserAnimationsModule
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
