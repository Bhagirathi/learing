import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InteractionService {
 private teacherMsgSource = new Subject<string>();
 teacherMsg$ = this.teacherMsgSource.asObservable(); //declairing a new observable 
  constructor() { }
  sendMessage(message:string)//method is created to accept the msg frm teachercomponent and push the msg using observable
  {
    this.teacherMsgSource.next(message); //next method used to send the message 
  }

}
