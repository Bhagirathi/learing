import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {LocalStorageService} from 'angular-2-local-storage';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private router: Router, private localStorage: LocalStorageService) {
  }

  canActivate() {
    if (!this.localStorage.get('userLoggedIn')) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}
