import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LoginRequest} from '../models/requestModel/login';
import {RegisterRequest} from '../models/requestModel/register';
import{productRequest} from '../models/requestModel/productRequest';


 const apiServer = 'http://backbone-sails.herokuapp.com';
//const apiServer2 = 'http://localhost:1337';
const apiServer1 = 'http://192.168.3.23:1337';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  constructor(private httpClient: HttpClient) {
  }
  login = (requestBody: LoginRequest) => {
    const url = apiServer + '/auth/signin';
    return this.httpClient.post(url, requestBody);
  };
  // login =(requestBody:LoginRequest)=> {
  //   const url= apiServer1+'/User/userLogin';
  //   // console.log(requestBody);

  //   return this.httpClient.post(url,requestBody); //testing purpose

  // };
  userdtl = (token) => {
    const url = apiServer + '/user/detail';
    
    const headerOptions = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token
      })
    };
    return this.httpClient.get(url,headerOptions);
    
  };
  register = (requestBody: RegisterRequest) => {
    const url = apiServer + '/auth/register';
    return this.httpClient.post(url, requestBody);
  };
  getDepartments = () => {
    const url = apiServer + '/public/departmentslist';
    return this.httpClient.get(url);
  };
  addCategory = (data: any, token) => {
    const url = apiServer + '/category/create';
    const headerOptions = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token
      })
    };
    return this.httpClient.post(url, data, headerOptions);
  };
  getCategoryList = (token) => {
    const url = apiServer + '/category/getlist';
    const headerOptions = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token
      })
    };
    return this.httpClient.get(url, headerOptions);
  };
  deleteCategoryList=(data, token)=>{
    
    const url= apiServer + '/category/delete';
    const headerOptions={
      headers:new HttpHeaders({Authorization:'Bearer '+ token}) //give space in Bearer
      };
      return this.httpClient.post(url,data,headerOptions);
  };
  addProduct=(requestBody:productRequest, token)=>{
    const url= apiServer + '/product/create';
    const headerOptions= {
      headers:new HttpHeaders({
        Authorization: 'Bearer ' + token
      })
    };
    return this.httpClient.post(url,requestBody,headerOptions);
  }
  getProduct=(token)=>{
    const url= apiServer + '/product/get';
    const headerOptions={
      headers:new HttpHeaders({Authorization: 'Bearer ' + token })
    };
    return this.httpClient.post(url,{},headerOptions);
  }
  deleteProduct=(data,token)=>{
    const url= apiServer + '/product/delete';
    const headerOptions= {
      headers: new HttpHeaders({Authorization:'Bearer '+ token})
    };
    return this.httpClient.post(url,data,headerOptions);
  }
  uploadImage = (token, data) => {
    const url = apiServer1 + '/media/upload';
    const headerOptions = {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token
      })
    };
    return this.httpClient.post(url, data, headerOptions);
  };


}
