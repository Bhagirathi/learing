
class ApiResponse {
   status: string;
   message: string;
  data: any;
}

export default ApiResponse;
