export class RegisterRequest {
  username: string;
  password: string;
  name: string;
  contact: string;
  department: string;
}
