import { Component, OnInit, Input } from '@angular/core';
import {Component1Component} from '../component1/component1.component';
import { InteractionService } from 'src/app/services/interaction.service';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';

@Component({
  selector: 'app-component2',
  templateUrl: './component2.component.html',
  styleUrls: ['./component2.component.css']
})
export class Component2Component implements OnInit {

  message:string;

  constructor( private interactionService:InteractionService) { }

  ngOnInit() {
    this.interactionService.teacherMsg$.subscribe(Message =>{
      if (Message=='GoodMorning')
      {
        this.message = Message;
        alert(Message);
      
      
      }
      else if(Message=='WelDone'){
        alert('Thank You Teacher');
      }
    });
  }

}