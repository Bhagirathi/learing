import { Component, OnInit,Input } from '@angular/core';
import {Component2Component} from '../component2/component2.component';
import { InteractionService } from 'src/app/services/interaction.service';

@Component({
  selector: 'app-component1',
  templateUrl: './component1.component.html',
  styleUrls: ['./component1.component.css']
})
export class Component1Component implements OnInit {

  constructor( private interactionService:InteractionService) { }

  ngOnInit() {
  }
  greetStudent(){
    this.interactionService.sendMessage('GoodMorning');
  }
  appriciatestudent(){
    this.interactionService.sendMessage('WelDone');
  }
}
