import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginPageComponent} from '../pages/login-page/login-page.component';
import {RegistrationPageComponent} from '../pages/registration-page/registration-page.component';
import {ForgetPasswordPageComponent} from '../pages/forget-password-page/forget-password-page.component';
import {HomePageComponent} from '../pages/home-page/home-page.component';
import {DashboardPageComponent} from '../pages/dashboard-page/dashboard-page.component';
import {ButtonPageComponent} from '../pages/button-page/button-page.component';
import {CategoriesPageComponent} from '../pages/categories-page/categories-page.component';
import {AuthGuardService} from '../services/auth-guard.service';
import{ChatPageComponent} from '../pages/chat-page/chat-page.component';
import{CatagoryComponent} from '../pages/catagory/catagory.component';
import {AddnewproductComponent} from '../pages/addnewproduct/addnewproduct.component';
import{ProductlistComponent} from '../pages/productlist/productlist.component';
import{RegisterComponent} from '../pages/register/register.component';
import{FileUploadComponent} from '../pages/file-upload/file-upload.component';
import{GmapComponent} from '../pages/gmap/gmap.component';


const MainRouter: Routes = [
  {path: 'login', component: LoginPageComponent},
  {path: 'register', component: RegistrationPageComponent},
  {path: 'forgetpwd', component: ForgetPasswordPageComponent},
  {path: 'home', component: HomePageComponent, canActivate: [AuthGuardService], children: [
      {path: 'dashboard', component: DashboardPageComponent},
      {path: 'button', component: ButtonPageComponent},
       {path: 'categories', component: CategoriesPageComponent},
       {path:'chat',component:ChatPageComponent},
       {path:'catagory',component:CatagoryComponent},
       {path:'addproduct',component:AddnewproductComponent},
       {path:'viewproduct',component:ProductlistComponent},
       {path:'registration',component:RegisterComponent},
      {path:'file-upload',component:FileUploadComponent},
      {path:'gmap',component:GmapComponent},
       {path: '**', redirectTo: 'dashboard'}
    ]},
    
  {path: '**', redirectTo: 'login'}
];

@NgModule({
  imports  : [
    CommonModule,
    RouterModule.forRoot(MainRouter,
      {enableTracing: false })
  ],
  declarations: [],
  providers: []
})
export class MainRoutingModule {}
