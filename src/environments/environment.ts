// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC6CQBRMgyPPGfXFNSe_4qGD5KlnOBwOTk",
    authDomain: "authdemo-177a0.firebaseapp.com",
    databaseURL: "https://authdemo-177a0.firebaseio.com",
    projectId: "authdemo-177a0",
    storageBucket: "authdemo-177a0.appspot.com",
    messagingSenderId: "115003411359"
 }
 
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
